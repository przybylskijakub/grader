package pl.air.grader.model;

import java.util.ArrayList;
import java.util.List;

public class Teacher extends User {

    private List<Subject> subjects = new ArrayList<Subject>();

    public Teacher(String login, String password, String firstName, String lastName, List<Subject> subjects) {
        super(login, password, firstName, lastName);
        this.subjects = subjects;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

//    public void addSubject(Subject subject) {
//        subjects.add(subject);
//    }

}
