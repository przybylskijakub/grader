package pl.air.grader.model;

import java.util.ArrayList;
import java.util.List;

public class Student extends User {

    private List<Grade> grades = new ArrayList<Grade>();

    public Student(String login, String password, String firstName, String lastName) {
        super(login, password, firstName, lastName);
    }

    public List<Grade> getGrades() {
        return grades;
    }

    public void addGrade(Grade grade) {
        grades.add(grade);
    }
}
