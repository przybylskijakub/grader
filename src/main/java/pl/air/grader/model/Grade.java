package pl.air.grader.model;

public class Grade extends Id {

    private int val;
    private String type;
    private Subject subject;

    public Grade(int val, String type, Subject subject) {
        this.val = val;
        this.type = type;
        this.subject = subject;
    }

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

}
