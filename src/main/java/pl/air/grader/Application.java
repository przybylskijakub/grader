package pl.air.grader;

import pl.air.grader.service.MenuService;

public class Application {
    public static void main(String[] args) {
        MenuService menu = new MenuService();
        menu.greeting();
    }
}
