package pl.air.grader.dao;

import pl.air.grader.model.Student;
import pl.air.grader.repo.GraderRepo;

import java.util.Collection;
import java.util.Map;

public class StudentDAO extends DAO {

    public StudentDAO(GraderRepo repo) { super(repo); }

    public Student get(long id) {
        return getTable().get(id);
    }

    public Collection<Student> list() { return getTable().values(); }

    public long save(Student one) {
        long id = one.getId();
        getTable().put(id, one);
        return id;
    }

    public void update(Student one) {
        long id = one.getId();
        getTable().put(id, one);
    }

    public void delete(long id) { getTable().remove(id); }

    private Map<Long, Student> getTable() { return repo.getStudents(); }
}
