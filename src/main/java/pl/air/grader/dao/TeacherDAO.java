package pl.air.grader.dao;

import pl.air.grader.model.Teacher;
import pl.air.grader.repo.GraderRepo;

import java.util.Collection;
import java.util.Map;

public class TeacherDAO extends DAO{
    
    public TeacherDAO(GraderRepo repo) { super(repo); }

    public Teacher get(long id) {
        return getTable().get(id);
    }

    public Collection<Teacher> list() { return getTable().values(); }

    public long save(Teacher one) {
        long id = one.getId();
        getTable().put(id, one);
        return id;
    }

    public void update(Teacher one) {
        long id = one.getId();
        getTable().put(id, one);
    }

    public void delete(long id) { getTable().remove(id); }

    private Map<Long, Teacher> getTable() { return repo.getTeachers(); }
}
