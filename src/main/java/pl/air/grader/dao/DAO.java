package pl.air.grader.dao;

import pl.air.grader.repo.GraderRepo;

public abstract class DAO {

    protected GraderRepo repo;

    public DAO(GraderRepo repo) {
        this.repo = repo;
    }
}
