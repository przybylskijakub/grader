package pl.air.grader.dao;

import pl.air.grader.model.User;
import pl.air.grader.repo.GraderRepo;

import java.util.Collection;
import java.util.Map;

public class UserDAO extends DAO {

    public UserDAO(GraderRepo repo) { super(repo); }

    public User get(long id) {
        return getTable().get(id);
    }

    public Collection<User> list() { return getTable().values(); }

    public long save(User one) {
        long id = one.getId();
        getTable().put(id, one);
        return id;
    }

    public void update(User one) {
        long id = one.getId();
        getTable().put(id, one);
    }

    public void delete(long id) { getTable().remove(id); }

    private Map<Long, User> getTable() { return repo.getUsers(); }
}
