package pl.air.grader.dao;

import pl.air.grader.model.Subject;
import pl.air.grader.repo.GraderRepo;

import java.util.Collection;
import java.util.Map;

public class SubjectDAO extends DAO{

    public SubjectDAO(GraderRepo repo) {
        super(repo);
    }

    public Subject get(long id) {
        //Map<Long, Subject> authors = repo.getSubjects();
        return getTable().get(id);
    }

    public Collection<Subject> list() {
        return getTable().values();
    }

    public long save(Subject one) {
        long id = one.getId();
        getTable().put(id, one);
        return id;
    }

    public void update(Subject one) {
        long id = one.getId();
        getTable().put(id, one);
    }

    public void delete(long id) {
        getTable().remove(id);
    }

    private Map<Long, Subject> getTable() {
        return repo.getSubjects();
    }
}
