package pl.air.grader.dao;

import pl.air.grader.model.Grade;
import pl.air.grader.repo.GraderRepo;

import java.util.Collection;
import java.util.Map;

public class GradeDAO extends DAO {

    public GradeDAO(GraderRepo repo) { super(repo); }

    public Grade get(long id) {
        return getTable().get(id);
    }

    public Collection<Grade> list() { return getTable().values(); }

    public long save(Grade one) {
        long id = one.getId();
        getTable().put(id, one);
        return id;
    }

    public void update(Grade one) {
        long id = one.getId();
        getTable().put(id, one);
    }

    public void delete(long id) { getTable().remove(id); }

    private Map<Long, Grade> getTable() { return repo.getGrades(); }
}
