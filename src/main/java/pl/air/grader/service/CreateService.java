package pl.air.grader.service;

import pl.air.grader.dao.*;
import pl.air.grader.model.*;
import pl.air.grader.repo.GraderRepo;

import java.util.ArrayList;
import java.util.List;

public class CreateService {

    private GraderRepo repo;

    public CreateService(GraderRepo repo) { this.repo = repo; }

    private List<Subject> subjectList1 = new ArrayList<Subject>();
    private List<Subject> subjectList2 = new ArrayList<Subject>();
    private List<Subject> subjectList3 = new ArrayList<Subject>();

    public User createUser(String login, String password, String firstName, String lastName) {
        User one = new User(login, password, firstName, lastName);
        UserDAO dao = new UserDAO(repo);
        dao.save(one);
        return one;
    }

    public Teacher createTeacher(String login, String password, String firstName, String lastName, List<Subject> subjects) {
        Teacher one = new Teacher(login, password, firstName, lastName, subjects);
        TeacherDAO dao = new TeacherDAO(repo);
        dao.save(one);
        return one;
    }

    public Student createStudent(String login, String password, String firstName, String lastName) {
        Student one = new Student(login, password, firstName, lastName);
        StudentDAO dao = new StudentDAO(repo);
        dao.save(one);
        return one;
    }

    public Subject createSubject(String name, Boolean isMandatory) {
        Subject one = new Subject(name, isMandatory);
        SubjectDAO dao = new SubjectDAO(repo);
        dao.save(one);
        return one;
    }

    public Grade createGrade(int val, String type, Subject subject) {
        Grade one = new Grade(val, type, subject);
        GradeDAO dao = new GradeDAO(repo);
        dao.save(one);
        return one;
    }

    public void createData() {
        Subject math = createSubject("Matematyka", true);
        Subject polish = createSubject("Jezyk polski", true);
        Subject biology = createSubject("Biologia", true);
        Subject chemistry = createSubject("Chemia", true);
        Subject physics = createSubject("Fizyka", true);
        Subject religion = createSubject("Religia", false);
        Subject french = createSubject("Jezyk francuski", false);

        subjectList1.add(math);
        subjectList1.add(chemistry);

        subjectList2.add(polish);
        subjectList2.add(french);
        subjectList2.add(religion);

        subjectList3.add(biology);
        subjectList3.add(physics);

        Teacher jannowak = createTeacher("janko911", "123456", "Jan", "Nowak", subjectList1);
        Teacher mariakowalska = createTeacher("maria1949", "titanic888", "Maria", "Kowalska", subjectList2);
        Teacher adamwozniak = createTeacher("woo123", "qwerty", "Adam", "Wozniak", subjectList3);

        Student szymonpszypecki = createStudent("szymko111", "wiedzmin3", "Szymon", "Pszypecki");
        Student jakublebronski = createStudent("lebron21", "haslo123", "Jakub", "Lebronski");
        Student janpiatkowski = createStudent("myst89", "dcsg999", "Jan", "Piatkowski");
        Student mikolajjasztrzabski = createStudent("jastrzab", "surykatka", "Mikolaj", "Jastrzabski");
        Student leonchcial = createStudent("pccc1918", "vaifuuu", "Leon", "Chcial");
        Student mateuszwojcicki = createStudent("mati2004", "efens", "Mateusz", "Wojcicki");
        Student amadeuszdzikowski = createStudent("dzik07", "chrum", "Amadeusz", "Dzikowski");

        User admin = createUser("admin", "admin1", "Jan", "Kowalski");
        User mod = createUser("moderator", "test123", "Marcin", "Deczitowski");
        User dyrektor = createUser("dyrektorszkoly", "gimnazjum2", "Bartosz", "Kurkowski");

        Grade grade1 = createGrade(1, "sprawdzian", math);
        Grade grade2 = createGrade(4, "kartkowka", math);
        Grade grade3 = createGrade(5, "zadanie domowe", math);
        Grade grade4 = createGrade(3, "kartkowka", math);
        Grade grade5 = createGrade(3, "kartkowka", math);
        Grade grade6 = createGrade(4, "kartkowka", math);
        Grade grade7 = createGrade(4, "sprawdzian", math);
        Grade grade8 = createGrade(4, "sprawdzian", math);
        Grade grade9 = createGrade(5, "sprawdzian", chemistry);
        Grade grade10 = createGrade(1, "sprawdzian", chemistry);
        Grade grade11 = createGrade(2, "inne", chemistry);
        Grade grade12 = createGrade(5, "zadanie domowe", chemistry);
        Grade grade13 = createGrade(5, "zadanie domowe", chemistry);
        Grade grade14 = createGrade(3, "zadanie domowe", chemistry);
        Grade grade15 = createGrade(5, "inne", chemistry);

        szymonpszypecki.addGrade(grade1);
        szymonpszypecki.addGrade(grade2);
        szymonpszypecki.addGrade(grade3);
        szymonpszypecki.addGrade(grade4);
        szymonpszypecki.addGrade(grade5);
        szymonpszypecki.addGrade(grade6);
        szymonpszypecki.addGrade(grade7);
        szymonpszypecki.addGrade(grade8);
        szymonpszypecki.addGrade(grade9);
        szymonpszypecki.addGrade(grade10);
        szymonpszypecki.addGrade(grade11);
        szymonpszypecki.addGrade(grade12);
        szymonpszypecki.addGrade(grade13);
        szymonpszypecki.addGrade(grade14);
        szymonpszypecki.addGrade(grade15);
    }
}
