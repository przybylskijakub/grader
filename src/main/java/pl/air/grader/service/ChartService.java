package pl.air.grader.service;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import pl.air.grader.model.Grade;
import pl.air.grader.model.Student;

public class ChartService {

    public void displayPieChart(Student loggedStudent) {
        if (!loggedStudent.getGrades().isEmpty()) {
            // create a dataset...
            DefaultPieDataset dataset = new DefaultPieDataset();
            dataset.setValue("1.0", calculateGradeCount(loggedStudent)[0]);
            dataset.setValue("2.0", calculateGradeCount(loggedStudent)[1]);
            dataset.setValue("3.0", calculateGradeCount(loggedStudent)[2]);
            dataset.setValue("4.0", calculateGradeCount(loggedStudent)[3]);
            dataset.setValue("5.0", calculateGradeCount(loggedStudent)[4]);

            // create a chart...
            JFreeChart chart = ChartFactory.createPieChart(
                    "Wykres kołowy",
                    dataset,
                    true, // legend?
                    true, // tooltips?
                    false // URLs?
            );

            // create and display a frame...
            ChartFrame frame = new ChartFrame("Pie Chart", chart);
            frame.pack();
            frame.setVisible(true);
        }else{
            System.out.println("Nie posiadasz obecnie zadnych ocen.");
        }
    }

    public void displayBarChart(Student loggedStudent) {
        if (!loggedStudent.getGrades().isEmpty()) {
            // create a dataset...
            DefaultCategoryDataset dataset = new DefaultCategoryDataset();

            for (int i = 0; i < 5; i++) {
                String columnKey = Integer.toString(i + 1);
                dataset.setValue(calculateGradeCount(loggedStudent)[i], "Liczba Ocen", columnKey);
            }

            // create a chart...
            JFreeChart chart = ChartFactory.createBarChart(
                    "Wykres słupkowy",
                    "Wartosc Oceny",
                    "Liczba Ocen",
                    dataset,
                    PlotOrientation.VERTICAL,
                    true, // legend?
                    true, // tooltips?
                    false // URLs?
            );

            // create and display a frame...
            ChartFrame frame = new ChartFrame("Bar Chart", chart);
            frame.pack();
            frame.setVisible(true);
        }else{
            System.out.println("Nie posiadasz obecnie zadnych ocen.");
        }
    }

    public int[] calculateGradeCount(Student loggedStudent){
        int[] gradeCount = new int [5];

        for (int i = 0; i < gradeCount.length; i++){
            for (final Grade grade : loggedStudent.getGrades()) {
                if (grade.getVal() == i+1){
                    gradeCount[i]++;
                }
            }
        }

        return gradeCount;
    }
}
