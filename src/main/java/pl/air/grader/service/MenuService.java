package pl.air.grader.service;

import pl.air.grader.dao.GradeDAO;
import pl.air.grader.dao.StudentDAO;
import pl.air.grader.dao.TeacherDAO;
import pl.air.grader.model.*;
import pl.air.grader.repo.GraderRepo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class MenuService {

    private Scanner input = new Scanner(System.in);
    private GraderRepo repo = new GraderRepo();
    CreateService cs = new CreateService(repo);
    ChartService ch = new ChartService();

    public MenuService(){
        cs.createData();
    }

    public void greeting(){
        String login;
        String password;

        System.out.println("Witaj w systemie eGrader!");
        System.out.println("Podaj login:");
        login = input.next();
        System.out.println("Podaj haslo:");
        password = input.next();

        loginAttempt(login, password);
    }

    public void loginAttempt(String login, String password){

        for(Map.Entry<Long, Teacher> teacherEntry: repo.getTeachers().entrySet()) {
            if (login.equals(teacherEntry.getValue().getLogin()) && password.equals(teacherEntry.getValue().getPassword())) {
                System.out.println("Zalogowano na konto nauczyciela.");
                Teacher loggedTeacher = teacherEntry.getValue();
                teacherMenu(loggedTeacher);
            }
        }

        for(Map.Entry<Long, Student> studentEntry: repo.getStudents().entrySet()) {
            if (login.equals(studentEntry.getValue().getLogin()) && password.equals(studentEntry.getValue().getPassword())) {
                System.out.println("Zalogowano na konto ucznia!");
                Student loggedStudent = studentEntry.getValue();
                studentMenu(loggedStudent);
            }
        }

        for(Map.Entry<Long, User> userEntry: repo.getUsers().entrySet()) {
            if (login.equals(userEntry.getValue().getLogin()) && password.equals(userEntry.getValue().getPassword())) {
                System.out.println("Zalogowano na konto z uprawnieniami specjalnymi!");
                User loggedUser = userEntry.getValue();
                userMenu(loggedUser);
            }
        }

        System.out.println("Nieprawidlowy login i/lub haslo!");
        greeting();
    }

    public void teacherMenu(Teacher loggedTeacher){
        System.out.println("Zalogowany jako " + loggedTeacher.getFirstName() + " " + loggedTeacher.getLastName() + ". Co chcesz zrobic?");

        System.out.println("1 - wpisz ocene");
        System.out.println("2 - usun ocene");
        System.out.println("3 - wyswietl raport ocen dla danego ucznia");
        System.out.println("4 - wyloguj sie");

        int x = input.nextInt();

        switch (x) {
            case 1:  assignGrade(loggedTeacher);
                break;
            case 2:  deleteGrade(loggedTeacher);
                break;
            case 3:  browseGrades(loggedTeacher);
                break;
            case 4:  greeting();
                break;
        }
    }

    public void studentMenu(Student loggedStudent){
        System.out.println("Zalogowany jako " + loggedStudent.getFirstName() + " " + loggedStudent.getLastName() + ". Co chcesz zrobic?");

        System.out.println("1 - wyswietl moje oceny w formie listy");
        System.out.println("2 - wyswietl moje oceny w formie wykresu kolowego");
        System.out.println("3 - wyswietl moje oceny w formie wykresu slupkowego");
        System.out.println("4 - wyloguj");

        int x = input.nextInt();

        switch (x) {
            case 1:  viewMyGrades(loggedStudent);
                break;
            case 2:  ch.displayPieChart(loggedStudent);
                studentMenu(loggedStudent);
                break;
            case 3:  ch.displayBarChart(loggedStudent);
                studentMenu(loggedStudent);
                break;
            case 4:  greeting();
                break;
        }
    }

    public void userMenu(User loggedUser){
        System.out.println("Zalogowany jako " + loggedUser.getFirstName() + " " + loggedUser.getLastName() + ". Co chcesz zrobic?");

        System.out.println("1 - zarejestruj nowego ucznia");
        System.out.println("2 - zarejestruj nowego nauczyciela");
        System.out.println("3 - usun konto ucznia");
        System.out.println("4 - usun konto nauczyciela");
        System.out.println("5 - wyloguj");

        int x = input.nextInt();

        switch (x) {
            case 1:  registerStudent(loggedUser);
                break;
            case 2:  registerTeacher(loggedUser);
                break;
            case 3:  deleteStudent(loggedUser);
                break;
            case 4:  deleteTeacher(loggedUser);
                break;
            case 5:  greeting();
                break;
        }
    }

    public void registerStudent(User loggedUser){
        System.out.println("Wprowadz login nowego ucznia:");
        String registeredLogin = input.next();
        System.out.println("Wprowadz haslo nowego ucznia:");
        String registeredPassword = input.next();
        System.out.println("Wprowadz imie nowego ucznia:");
        String registeredName = input.next();
        System.out.println("Wprowadz nazwisko nowego ucznia:");
        String registeredLastName = input.next();

        cs.createStudent(registeredLogin, registeredPassword, registeredName, registeredLastName);

        System.out.println("Zarejestrowano nowego ucznia!");

        userMenu(loggedUser);
    }

    public void registerTeacher(User loggedUser){
        long x;
        List<Subject> registeredSubjectList = new ArrayList<Subject>();

        System.out.println("Wprowadz login nowego nauczyciela:");
        String registeredLogin = input.next();
        System.out.println("Wprowadz haslo nowego nauczyciela:");
        String registeredPassword = input.next();
        System.out.println("Wprowadz imie nowego nauczyciela:");
        String registeredName = input.next();
        System.out.println("Wprowadz nazwisko nowego nauczyciela:");
        String registeredLastName = input.next();

        System.out.println("Wybierz po numerze przedmioty dla nowego nauczyciela z ponizszej listy.");
        System.out.println("Wprowadz 0 (zero) aby zakonczyc dodawanie przedmiotow.");

        for (long i = 1; i < repo.getSubjects().size(); i++) {
            System.out.println(i + " - " + repo.getSubjects().get(i).getName());
        }

        do {
            x = input.nextLong();
            if (x == 0){
                break;
            }
            registeredSubjectList.add(repo.getSubjects().get(x));
            System.out.println("Dodano przedmiot!");
        } while(x != 0);

        System.out.println("Zakonczono wprowadzanie przedmiotow!");

        cs.createTeacher(registeredLogin, registeredPassword, registeredName, registeredLastName, registeredSubjectList);

        System.out.println("Zarejestrowano nowego nauczyciela!");

        userMenu(loggedUser);
    }

    public void deleteStudent(User loggedUser){
        System.out.println("Ktorego ucznia chcesz usunac z systemu?");

        for(Map.Entry<Long, Student> studentEntry: repo.getStudents().entrySet()){
            System.out.println(studentEntry.getKey() + " - " + studentEntry.getValue().getFirstName() + " " + studentEntry.getValue().getLastName());
        }

        long id = input.nextLong();
        StudentDAO dao = new StudentDAO(repo);
        dao.delete(id);

        System.out.println("Usunieto ucznia!");

        userMenu(loggedUser);
    }

    public void deleteTeacher(User loggedUser){
        System.out.println("Ktorego nauczyciela chcesz usunac z systemu?");

        for(Map.Entry<Long, Teacher> teacherEntry: repo.getTeachers().entrySet()){
            System.out.println(teacherEntry.getKey() + " - " + teacherEntry.getValue().getFirstName() + " " + teacherEntry.getValue().getLastName());
        }

        long id = input.nextLong();
        TeacherDAO dao = new TeacherDAO(repo);
        dao.delete(id);

        System.out.println("Usunieto nauczyciela!");

        userMenu(loggedUser);
    }

    public void assignGrade(Teacher loggedTeacher){
        System.out.println("Z jakiego przedmiotu chcesz wpisac ocene?");

        for (int i = 0; i < loggedTeacher.getSubjects().size(); i++) {
            System.out.println(i+1 + " - " + loggedTeacher.getSubjects().get(i).getName());
        }

        int x = input.nextInt();
        Subject currentSubject = loggedTeacher.getSubjects().get(x-1);

        System.out.println("Komu chcesz wpisac ocene? Wpisz ID uzytkownika.");

        for(Map.Entry<Long, Student> gradedStudent: repo.getStudents().entrySet()){
            System.out.println(gradedStudent.getKey() + " - " + gradedStudent.getValue().getFirstName() + " " + gradedStudent.getValue().getLastName());
        }

        long studentPointer = input.nextLong();
        Student currentStudent = repo.getStudents().get(studentPointer);

        System.out.println("Z czego jest wpisywana ocena?");
        System.out.println("1 - sprawdzian");
        System.out.println("2 - kartkowka");
        System.out.println("3 - zadanie domowe");
        System.out.println("4 - inne");

        int y = input.nextInt();

        String currentType;

        switch (y) {
            case 1:  currentType = "sprawdzian";
                break;
            case 2:  currentType = "kartkowka";
                break;
            case 3:  currentType = "zadanie domowe";
                break;
            case 4:  currentType = "inne";
                break;
            default: currentType = "inne";
                break;
        }

        System.out.println("Jaka ocene chcesz wpisac? Wybierz wartosc od 1 do 6.");

        int z = input.nextInt();

        if (z < 1 || z > 6) {
            do {
                System.out.println("Wprowadz poprawna wartosc!");
                z = input.nextInt();
            } while(z < 1 || z > 6);
        }else{
            Grade currentGrade = new Grade(z, currentType, currentSubject);
            currentStudent.addGrade(currentGrade);
            System.out.println("Zapisano ocene!");
            teacherMenu(loggedTeacher);
        }

    }

    public void deleteGrade(Teacher loggedTeacher) {
        System.out.println("Z jakiego przedmiotu chcesz usunac ocene?");

        for (int i = 0; i < loggedTeacher.getSubjects().size(); i++) {
            System.out.println(i+1 + " - " + loggedTeacher.getSubjects().get(i).getName());
        }

        int x = input.nextInt();
        Subject currentSubject = loggedTeacher.getSubjects().get(x-1);

        System.out.println("Komu chcesz usunac ocene? Wpisz ID uzytkownika.");

        for (Map.Entry<Long, Student> gradedStudent: repo.getStudents().entrySet()){
            System.out.println(gradedStudent.getKey() + " - " + gradedStudent.getValue().getFirstName() + " " + gradedStudent.getValue().getLastName());
        }

        long studentPointer = input.nextLong();
        Student currentStudent = repo.getStudents().get(studentPointer);

        if (!currentStudent.getGrades().isEmpty()) {
            System.out.println("Oto dostepne oceny ucznia " + currentStudent.getFirstName() + " " + currentStudent.getLastName() + ":");
//            for (final Grade grade : currentStudent.getGrades()) {
//                if (currentSubject == grade.getSubject()){
//                    System.out.println("ID: " + grade.getId() + " - " + grade.getVal() + " - " + grade.getSubject().getName() + " (" + grade.getType() + ")");
//                }
//            }
            for(Map.Entry<Long, Student> studentEntry: repo.getStudents().entrySet()){
                if (currentStudent == studentEntry.getValue()){
                    for (int i = 0; i < studentEntry.getValue().getGrades().size(); i++)
                    System.out.println(studentEntry.getValue().getGrades().get(i).getId() + " - " + studentEntry.getValue().getGrades().get(i).getVal() + " - " + studentEntry.getValue().getGrades().get(i).getSubject().getName() + " (" + studentEntry.getValue().getGrades().get(i).getType() + ")");
                }
            }
        }else{
            System.out.println("Brak dostepnych ocen do usuniecia.");
            teacherMenu(loggedTeacher);
        }

        System.out.println("Ktora z nich chcesz usunac? Wpisz ID wybranej oceny.");

        long id = input.nextLong();
        GradeDAO dao = new GradeDAO(repo);
        dao.delete(id);

        System.out.println("Pomyslnie usunieto ocene!");

        teacherMenu(loggedTeacher);
    }

    public void browseGrades(Teacher loggedTeacher){
        System.out.println("Dla jakiego ucznia wyswietlic raport ocen?");

        for(Map.Entry<Long, Student> previewedStudent: repo.getStudents().entrySet()){
            System.out.println(previewedStudent.getKey() + " - " + previewedStudent.getValue().getFirstName() + " " + previewedStudent.getValue().getLastName());
        }

        long studentPointer = input.nextLong();
        Student currentStudent = repo.getStudents().get(studentPointer);

        if (!currentStudent.getGrades().isEmpty()){
            System.out.println("Oto oceny ucznia " + currentStudent.getFirstName() + " " + currentStudent.getLastName() + ":");
        }else{
            System.out.println("Brak ocen.");
        }

        for (final Grade grade : currentStudent.getGrades()) {
            System.out.println(grade.getVal() + " - " + grade.getSubject().getName() + " (" + grade.getType() + ")");
        }

        teacherMenu(loggedTeacher);
    }

    public void viewMyGrades(Student loggedStudent){
        if (!loggedStudent.getGrades().isEmpty()){
            System.out.println("Ponizej znajduja sie obecne oceny ucznia " + loggedStudent.getFirstName() + " " + loggedStudent.getLastName() + ":");
            for (final Grade grade : loggedStudent.getGrades()) {
                System.out.println(grade.getVal() + " - " + grade.getSubject().getName() + " (" + grade.getType() + ")");
            }
        }else{
            System.out.println("Nie posiadasz obecnie zadnych ocen.");
        }

        studentMenu(loggedStudent);
    }
}
