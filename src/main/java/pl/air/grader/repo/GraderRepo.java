package pl.air.grader.repo;

import pl.air.grader.model.*;

import java.util.HashMap;
import java.util.Map;

public class GraderRepo {

    public GraderRepo() {
    }

    private Map<Long, User> users = new HashMap<Long, User>();
    private Map<Long, Teacher> teachers = new HashMap<Long, Teacher>();
    private Map<Long, Student> students = new HashMap<Long, Student>();
    private Map<Long, Subject> subjects = new HashMap<Long, Subject>();
    private Map<Long, Grade> grades = new HashMap<Long, Grade>();

    public Map<Long, User> getUsers() { return users; }
    public Map<Long, Teacher> getTeachers() { return teachers; }
    public Map<Long, Subject> getSubjects() { return subjects; }
    public Map<Long, Student> getStudents() { return students; }
    public Map<Long, Grade> getGrades() {return grades; }
}
